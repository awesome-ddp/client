ARG NODE_VERSION
ARG NGINX_VERSION

FROM node:${NODE_VERSION} as build-image

WORKDIR /app

COPY package*.json ./

RUN yarn

COPY . .

RUN yarn build

FROM nginx:${NGINX_VERSION} as final-image

COPY ./.nginx/nginx.conf /etc/nginx/nginx.conf

RUN rm -rf /usr/share/nginx/html/*

COPY --from=build-image /app/build /usr/share/nginx/html

ENTRYPOINT ["nginx", "-g", "daemon off;"]