ARG NODE_VERSION

FROM node:${NODE_VERSION}

WORKDIR /app

COPY package*.json ./

RUN yarn

COPY . .

CMD ["yarn", "start"]
