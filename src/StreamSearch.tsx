import SearchIcon from '@material-ui/icons/Search';
import { fade, Grid, styled, TextField } from '@material-ui/core';

const StreamSearchWrap = styled('div')(({ theme }) => ({
  margin: theme.spacing(1),
  padding: theme.spacing(1),
  position: 'relative',
  borderRadius: theme.shape.borderRadius,
  backgroundColor: fade(theme.palette.common.white, 0.15),
  '&:hover': {
    backgroundColor: fade(theme.palette.common.white, 0.25),
  },
  marginLeft: 0,
  [theme.breakpoints.up('sm')]: {
    marginLeft: theme.spacing(1),
    width: 'auto',
  },
}));

const StreamSearch = () => {
  return (
    <StreamSearchWrap className={'StreamSearch'}>
      <Grid container spacing={1} alignItems={'center'}>
        <Grid item>
          <SearchIcon />
        </Grid>
        <Grid item>
          <form>
            <TextField placeholder={'Search for a stream...'} />
          </form>
        </Grid>
      </Grid>
    </StreamSearchWrap>
  );
};

export default StreamSearch;
