import {
  AppBar,
  Box,
  Button,
  Grid,
  styled,
  Toolbar,
  Typography,
} from '@material-ui/core';
import StreamSearch from './StreamSearch';

const TopBar = () => {
  return (
    <div className={'TopBar'}>
      <AppBar position={'static'}>
        <Toolbar>
          <Grid container justify={'space-between'} alignItems={'center'}>
            <Grid item>
              <Box display={'flex'} alignItems={'center'}>
                <Typography variant={'h6'}>Awesome DDP</Typography>
                <StreamSearch />
              </Box>
            </Grid>
            <Grid item>
              <Button variant={'contained'} color={'secondary'}>
                Stream now
              </Button>
            </Grid>
          </Grid>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default TopBar;
