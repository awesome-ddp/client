import TopBar from './TopBar';
import Main from './Main';

const App = () => {
  return (
    <div className={'App'}>
      <TopBar />
      <Main />
    </div>
  );
};

export default App;
